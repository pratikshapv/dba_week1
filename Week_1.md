<img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0231EN-SkillsNetwork/labs/Db2/Lab%20-%20Db2%20System%20Tables/images/IDSNlogo.png" alt="IDSN logo" width="200" height="200">

# Hands-on Lab: MySQL System Tables using phpMyAdmin

**Estimated time needed:** 20 minutes

In this lab, you will learn how to access and work with system tables in the MySQL relational database management system. Every MySQL database contains system catalog tables that store metadata and definitions of the database objects such as tables, views, indexes, etc.

<br>

## Objectives

After completing this lab, you will be able to use MySQL to:

*   Explore the system catalog tables of your MySQL database
*   Query the system catalog to retrieve metadata from tables in your database
*   Query the system catalog to retrieve column metadata

# 

## Software Used in this Lab

In this lab, you will use <a href="https://www.mysql.com/?utm_medium=Exinfluencer&utm_source=Exinfluencer&utm_content=000026UJ&utm_term=10006555&utm_id=NA-SkillsNetwork-Channel-SkillsNetworkCoursesIBMDB0110ENSkillsNetwork24601058-2021-01-01">MySQL</a>. MySQL is a Relational Database Management System (RDBMS) designed to efficiently store, manipulate, and retrieve data.

<img src="https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/mysql.png" width="100" height="100">
<p></p>

To complete this lab you will utilize MySQL relational database service available as part of IBM Skills Network Labs (SN Labs) Cloud IDE. SN Labs is a virtual lab environment used in this course.

#

## Database Used in This Lab

The database used in this lab is an internal database. You will be working on a sample HR database. This HR database schema consists of 5 tables called EMPLOYEES, JOB_HISTORY, JOBS, DEPARTMENTS and LOCATIONS. Each table has a few rows of sample data. The following diagram shows the tables for the HR database:

![Sample HR Database Diagram](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0231EN-SkillsNetwork/labs/Db2/Lab%20-%20Db2%20System%20Tables/images/Sample_HR_DB.png)

## Exercise 1: Load the Database into MySQL using phpMyAdmin GUI tool.


1.Download the sample HR database by right-clicking the following link and saving as HR_Database_Create_Tables_Script.sql: [Sample HR Database](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBMDeveloperSkillsNetwork-DB0201EN-SkillsNetwork/labs/Labs_Coursera_V5/datasets/HR_Database/HR_Database_Create_Tables_Script.sql)


2.Go to **Terminal > New Terminal** to open a terminal from the side by side launched Cloud IDE.

![image](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/2.1.png)

3.Start MySQL service session in the Cloud IDE using the command below in the terminal. Find your MySQL service session password from the highlighted location of the terminal shown in the image below. Note down your MySQL service session password because you may need to use it later in the lab.

```
start_mysql
```

![image](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/2.2.png)

<br>

4.Copy your phpMyAdmin weblink from the highlighted location of the terminal shown in the image below. Past it into the address bar in a new tab of your web browser. This will open the phpMyAdmin tool.

![image](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/2.3.png)

<br>

5.You will see the phpMyAdmin GUI tool.

![image](https://cf-courses-data.s3.us.cloud-object-storage.appdomain.cloud/IBM-DB0110EN-SkillsNetwork/labs/Lab%20-%20Create%20Tables%20and%20Load%20Data%20in%20MySQL%20using%20phpMyAdmin/images/2.4.png)

<br>

6.In the tree-view, click **New** to create a new empty database. Then enter **HR_Database** as the name of the database and click **Create**.

The encoding will be left as **utf8mb4\_0900\_ai_ci**. UTF-8 is the most commonly used character encoding for content or data.


![image](./images/db1.PNG)  

7.Go to **Import** tab to load the data. Click **Choose File** and load the **HR_Database_Create_Tables_Script.sql** file from your local computer storage. The rest of the settings can be left as they are because you are importing a SQL script that is encoded with UTF-8.

Then click **Go**. Notification of import success will appear.  

![image](./images/db2.PNG)

After executing this SQL code, your MySQL server will now have a database containing several tables.

# Exercise 2: Access MySQL System Tables

Every MySQL database has a **CATALOG TABLE**. The catalog table contains one row for each table, view, or alias. Each table row indicates whether the object that it describes is a table, view, or alias, its name, who created it, the database that it belongs to, the table space it belongs to, and other information. The catalog table also has a COMMENTS column in which you can store your own information about the table in question.

In essence, the catalog table contains metadata about all other database objects in the MySQL server. Let's go ahead and see how you can access some of this useful metadata.

### Task A: Perform a Simple Query on System tables

1.GO to information_schema database , Click the **SQL** section of the phpMyAdmin tool.

Let's start off with the most basic query on the `SYSTEM TABLES` table by taking a look at the whole table. Enter the following code into the SQL editor and click **Go**.

```
SELECT * FROM TABLES;
```

![image](./images/task.A1.PNG)


### Task B: Query on System tables 

Let's do a slightly more advanced query on `SYSTEM TABLES` to retrieve the metadata for the tables you created for your sample HR_database. One way to do this is to query the table based on the `TABLE_SCHEMA` column, which stores the schema that the given table belongs to.

1.GO to information_schema database , Click the **SQL** section of the phpMyAdmin tool.

2.Enter the following SQL command to query the TABLES. 

```
SELECT * FROM TABLES WHERE TABLE_SCHEMA = 'HR_Database';
```

![image](./images/task.B1.PNG)


### Task C: Try Updating a System Table

In MySQL database, the system tables cannot be edited directly with `INSERT`, `UPDATE`, or `DELETE` statements. When you create, modify, or delete other objects in the database, the system tables are automatically updated to reflect the changes. Therefore, system tables are only for *retrieving* metadata about other objects in your database, you do not modify them directly.

The current schema name for five tables is HR_database, but suppose you wanted to modify the entries in SYSTEM TABLES to have a different schema name in those tables.

Let's see what happens if you try to modify the schema name in `TABLE_SCHEMA` column in `SYSTEM TABLES`.

1.GO to information_schema database , Click the **SQL** section of the phpMyAdmin tool.

2.Enter the following SQL command to query the TABLES. 

```
UPDATE TABLES SET TABLE_SCHEMA = 'HR_Database' 
WHERE TABLE_SCHEMA ='HR_Database1';
```

![image](./images/task.C1.PNG)

# Exercise 3: Try it yourself!

As mentioned in the previous exercise, MySQL system tables cannot be modified directly. Instead, they store metadata about all objects in the database and are automatically updated to reflect any changes made to them, including the creation or deletion of database objects.

You will now get an opportunity to apply what you learned and see this in action for yourself. In this practice exercise, use the SQL editor along with the steps given below.

1.**Try it yourself :** Query the `SYSTEM TABLES` by the schema name to display the five tables that belong to the sample HR database.


<details>
<summary><strong>Solution</strong> (Click Here)</summary>

```
SELECT * FROM TABLES WHERE TABLE_SCHEMA = 'HR_Database';
```

![image](./images/solution1.PNG)

</details>


2.**Try it yourself :** Try directly deleting the row corresponding to the EMPLOYEES table from the TABLE_SCHEMA system table.


<details>
<summary><strong>Solution</strong> (Click Here)</summary>

```
DELETE FROM TABLE_SCHEMA WHERE TABLE_NAME = 'EMPLOYEES';
```

![image](./images/solution2.PNG)
    
As you may have guessed, this command raises an error since we cannot use `DELETE` commands directly on system tables.

</details>

3.**Try it yourself:** Drop the EMPLOYEES table from the database.

<details>
<summary><strong>Solution</strong> (Click Here)</summary>

You can drop a table from a database by going to your created database section ,using a SQL command of the following form:

```
DROP TABLE EMPLOYEES;
```

![image](./images/solution3.1.PNG)

![image](./images/solution3.2.PNG)

    
</details>

4.**Try it yourself:** Once again, query the `SYSTEM TABLES`by schema name to confirm that the EMPLOYEES table was successfully dropped from the database.

<details>
<summary><strong>Solution</strong> (Click Here)</summary>
Go to MySQL information_schema database and write the following command;

```
SELECT * FROM TABLES WHERE TABLE_SCHEMA = 'HR_Database';
```

![image](./images/solution4.PNG)

As you can see, the SYSTEM TABLES is automatically updated to reflect that the EMPLOYEES table is dropped from the database. Now only 4 tables are present. You did not have to modify the SYSTEM TABLES directly.

</details>

#

## Conclusion

Congratulations on completing this hands-on lab! You now have some understanding and experience of working with the system tables of a MySQL server and are ready for the next step in developing your skill set as a database administrator.


#

## Author(s)


[Pratiksha Verma](https://www.linkedin.com/in/pratiksha-verma-6487561b1/)

#

## Changelog

| Date       | Version | Changed by      | Change Description      |
| ---------- | ------- | --------------- | ----------------------- |
| 2022-02-24 | 1.0     | Pratiksha verma |  Initial Version        |

## <h3 align="center"> IBM Corporation 2022. All rights reserved. <h3/>









